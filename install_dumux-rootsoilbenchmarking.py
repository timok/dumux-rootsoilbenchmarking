#!/usr/bin/env python3

#
# This installs the module dumux-rootsoilbenchmarking and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
#
#
# |      module name      |   branch name   |                 commit sha                 |         commit date         |
# |-----------------------|-----------------|--------------------------------------------|-----------------------------|
# |      dune-alugrid     |  origin/master  |  13f131263f9293d768fa5a542a9c52bd3e432c5f  |  2023-02-13 11:58:52 +0100  |
# |       dune-istl       |  origin/master  |  1623ad6e3b5a55a8b7343dfdcb61ac47bf821b2e  |  2023-02-12 20:05:08 +0000  |
# |       dune-grid       |  origin/master  |  c5fb2887295f98d4dd27fd5cfee4df0610ff5b46  |  2023-02-28 12:02:46 +0000  |
# |         dumux         |  origin/master  |  fb6f94fb65f02c90b686109193503d547a3ec61c  |  2023-04-16 13:41:14 +0000  |
# |  dune-localfunctions  |  origin/master  |  8f1cb33575204e5a245e9e6088e7a6471d3903a9  |  2023-03-09 08:08:07 +0000  |
# |     dune-geometry     |  origin/master  |  1a4f504d95f1a5ccb46b380c316da421b770c091  |  2023-02-28 09:20:52 +0000  |
# |      dune-subgrid     |  origin/master  |  e83f3f919c2602425467ed767f279bc9c356c436  |  2023-01-04 19:24:06 +0000  |
# |     dune-functions    |  origin/master  |  fd79ceb1cc17049d71496d8b11e014f5ac797d61  |  2023-03-09 17:45:09 +0000  |
# |     dune-foamgrid     |  origin/master  |  4845fbcac1bb814fc65173dfd2f4d1d58ce33863  |  2023-02-02 11:36:13 +0000  |
# |      dune-uggrid      |  origin/master  |  d0762d9b1c56048ab738eba80dbdd65604b26708  |  2022-11-28 16:10:22 +0000  |
# |      dune-common      |  origin/master  |  e4bf440b28a4d67aff2a6b356b0895140baf4b0c  |  2023-03-06 21:54:12 +0000  |
# |     dune-typetree     |  origin/master  |  d3345d13804d9b6a26973272d799ca345abfd49c  |  2022-12-01 09:56:36 +0000  |

import os
import sys
import subprocess

top = "."
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.split("/")[-1]
    if targetFolder.endswith(".git"):
        targetFolder = targetFolder[:-4]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dune-alugrid")
installModule("dune-alugrid", "https://gitlab.dune-project.org/extensions/dune-alugrid", "origin/master", "13f131263f9293d768fa5a542a9c52bd3e432c5f", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl", "origin/master", "1623ad6e3b5a55a8b7343dfdcb61ac47bf821b2e", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid", "origin/master", "c5fb2887295f98d4dd27fd5cfee4df0610ff5b46", )

print("Installing dumux")
installModule("dumux", "git@git.iws.uni-stuttgart.de:dumux-repositories/dumux.git", "origin/master", "fb6f94fb65f02c90b686109193503d547a3ec61c", )

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions", "origin/master", "8f1cb33575204e5a245e9e6088e7a6471d3903a9", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry", "origin/master", "1a4f504d95f1a5ccb46b380c316da421b770c091", )

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://gitlab.dune-project.org/extensions/dune-subgrid", "origin/master", "e83f3f919c2602425467ed767f279bc9c356c436", )

print("Installing dune-functions")
installModule("dune-functions", "https://gitlab.dune-project.org/staging/dune-functions", "origin/master", "fd79ceb1cc17049d71496d8b11e014f5ac797d61", )

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid", "origin/master", "4845fbcac1bb814fc65173dfd2f4d1d58ce33863", )

print("Installing dune-uggrid")
installModule("dune-uggrid", "https://gitlab.dune-project.org/staging/dune-uggrid", "origin/master", "d0762d9b1c56048ab738eba80dbdd65604b26708", )

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common", "origin/master", "e4bf440b28a4d67aff2a6b356b0895140baf4b0c", )

print("Installing dune-typetree")
installModule("dune-typetree", "https://gitlab.dune-project.org/staging/dune-typetree", "origin/master", "d3345d13804d9b6a26973272d799ca345abfd49c", )

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux-rootsoilbenchmarking/cmake.opts', 'all'],
    '.'
)
