## Content

Benchmark cases C1.1/1.2 from Schnepf (2020), https://doi.org/10.3389/fpls.2020.00316

Benchmark C1.2a/b using DuMux explicit interface method in
*   `test/benchmark_lupine`

Benchmark C1.1 using DuMux cylinder surface method in
*   `test/benchmark_single`

Note that both cases can have quite a long runtime.
The mesh for the explicit interface benchmark implementation has more than 8Mio grid elements.
Run on a multi-core machine with 30 cores or more
for decent performance (couple of hours runtime).
For this you need a [working multi-threading backend](https://dumux.org/docs/doxygen/master/running-in-parallel.html#sharedmemory-parallelism-and-multithreaded-applications).

## License

This project is licensed under the terms and conditions of the GNU General Public
License (GPL) version 3 or - at your option - any later version.
The GPL can be found under [GPL-3.0-or-later.txt](LICENSES/GPL-3.0-or-later.txt)
provided in the `LICENSES` directory located at the topmost of the source code tree.


## Version Information

This module works with Dune releases 2.9 and Dumux 3.7. The following
more recent versions have been used for testing:

|      module name      |   branch name   |                 commit sha                 |         commit date         |
|-----------------------|-----------------|--------------------------------------------|-----------------------------|
|      dune-alugrid     |  origin/master  |  13f131263f9293d768fa5a542a9c52bd3e432c5f  |  2023-02-13 11:58:52 +0100  |
|       dune-istl       |  origin/master  |  1623ad6e3b5a55a8b7343dfdcb61ac47bf821b2e  |  2023-02-12 20:05:08 +0000  |
|       dune-grid       |  origin/master  |  c5fb2887295f98d4dd27fd5cfee4df0610ff5b46  |  2023-02-28 12:02:46 +0000  |
|         dumux         |  origin/master  |  fb6f94fb65f02c90b686109193503d547a3ec61c  |  2023-04-16 13:41:14 +0000  |
|  dune-localfunctions  |  origin/master  |  8f1cb33575204e5a245e9e6088e7a6471d3903a9  |  2023-03-09 08:08:07 +0000  |
|     dune-geometry     |  origin/master  |  1a4f504d95f1a5ccb46b380c316da421b770c091  |  2023-02-28 09:20:52 +0000  |
|      dune-subgrid     |  origin/master  |  e83f3f919c2602425467ed767f279bc9c356c436  |  2023-01-04 19:24:06 +0000  |
|     dune-functions    |  origin/master  |  fd79ceb1cc17049d71496d8b11e014f5ac797d61  |  2023-03-09 17:45:09 +0000  |
|     dune-foamgrid     |  origin/master  |  4845fbcac1bb814fc65173dfd2f4d1d58ce33863  |  2023-02-02 11:36:13 +0000  |
|      dune-uggrid      |  origin/master  |  d0762d9b1c56048ab738eba80dbdd65604b26708  |  2022-11-28 16:10:22 +0000  |
|      dune-common      |  origin/master  |  e4bf440b28a4d67aff2a6b356b0895140baf4b0c  |  2023-03-06 21:54:12 +0000  |
|     dune-typetree     |  origin/master  |  d3345d13804d9b6a26973272d799ca345abfd49c  |  2022-12-01 09:56:36 +0000  |

## Installation

The installation procedure is done as follows:
Create a root folder, e.g. `DUMUX`, enter the previously created folder,
clone this repository and use the install script `install_dumux-rootsoilbenchmarking.py`
provided in this repository to install all dependent modules.

```sh
mkdir DUMUX
cd DUMUX
git clone git@git.iws.uni-stuttgart.de:timok/dumux-rootsoilbenchmarking.git dumux-rootsoilbenchmarking
./dumux-rootsoilbenchmarking/install_dumux-rootsoilbenchmarking.py
```

This will clone all modules into the directory `DUMUX`,
configure your module with `dunecontrol` and build tests.
For more detailed instruction in case this fails see [here](https://dumux.org/docs/doxygen/master/installation.html).
