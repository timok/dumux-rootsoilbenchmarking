#!/usr/bin/env python
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt

kx0 = np.array([[32,3.57E-01],[30,3.21E-01],[28,2.90E-01],[26,2.61E-01],[24,2.35E-01],[22,2.12E-01],[20,1.91E-01],
                [18,1.72E-01],[16,1.55E-01],[14,1.40E-01],[12,1.26E-01],[10,1.13E-01],[8,1.02E-01],[6,9.21E-02],
                [4,8.30E-02],[2,7.48E-02],[0,6.74E-02]])
kr0 = np.array([[32,5.17E-04],[30,5.43E-04],[28,5.70E-04],[26,5.99E-04],[24,6.30E-04],[22,6.62E-04],[20,6.95E-04],
                [18,7.30E-04],[16,7.67E-04],[14,8.06E-04],[12,8.47E-04],[10,8.90E-04],[8,9.35E-04],[6,9.83E-04],
                [4,1.03E-03],[2,1.09E-03],[0,1.14E-03]])
kx1 = np.array([[17,1.36E-02],[16,1.11E-02],[15,9.03E-03],[14,7.34E-03],[13,5.97E-03],[12,4.86E-03],[11,3.95E-03],
                [10,3.21E-03],[9,2.61E-03],[8,2.12E-03],[7,1.73E-03],[6,1.41E-03],[5,1.14E-03],[4,9.30E-04],
                [3,7.56E-04],[2,6.15E-04],[1,5.00E-04],[0,4.07E-04]])
kr1 = np.array([[17,1.58E-03],[16,1.67E-03],[15,1.77E-03],[14,1.87E-03],[13,1.98E-03],[12,2.09E-03],[11,2.21E-03],
                [10,2.34E-03],[9,2.48E-03],[8,2.62E-03],[7,2.77E-03],[6,2.93E-03],[5,3.10E-03],[4,3.28E-03],
                [3,3.47E-03],[2,3.67E-03],[1,3.89E-03],[0,4.11E-03]])

kx0[:,1] = kx0[:,1]/86400.0*1e-6/1000/9.81
kx1[:,1] = kx1[:,1]/86400.0*1e-6/1000/9.81
kr0[:,1] = kr0[:,1]/86400.0/1000/9.81
kr1[:,1] = kr1[:,1]/86400.0/1000/9.81

print(kx0[:,0][::-1], kx0[:,1][::-1])
print(kx1[:,0][::-1], kx1[:,1][::-1])
print(kr0[:,0][::-1], kr0[:,1][::-1])
print(kr1[:,0][::-1], kr1[:,1][::-1])

kx0_ = interpolate.interp1d(kx0[:,0][::-1], kx0[:,1][::-1])
kr0_ = interpolate.interp1d(kr0[:,0][::-1], kr0[:,1][::-1])
kx1_ = interpolate.interp1d(kx1[:,0][::-1], kx1[:,1][::-1], fill_value=np.max(kx1[:,1]), bounds_error = False)
kr1_ = interpolate.interp1d(kr1[:,0][::-1], kr1[:,1][::-1], fill_value=np.min(kr1[:,1]), bounds_error = False)

# Plot function
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 7))
age_ = np.linspace(0,17,100)
ax1.plot(age_,kx0_(age_), "b--")
ax1.plot(age_,kx1_(age_), "g--")
ax1.set_xlabel("age [days]")
ax1.set_ylabel("axial conductivity $k_z$ [cm$^3$/day]")
ax1.legend(["first order", "higher orders"])
ax2.plot(age_,kr0_(age_), "b--")
ax2.plot(age_,kr1_(age_), "g--")
ax2.set_ylabel("radial conductivity $k_r$ [1/day]")
ax2.set_xlabel("age [days]")
ax2.legend(["first order", "higher orders"])

fig.tight_layout()
plt.show()
