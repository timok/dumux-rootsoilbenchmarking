dune_add_test(NAME test_benchmark_proj
              SOURCES main.cc
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )"
              COMPILE_DEFINITIONS BULKTYPETAG=BulkBox LOWDIMTYPETAG=LowDimBox
              COMPILE_DEFINITIONS COUPLINGMODE=Embedded1d3dCouplingMode::Projection
              COMPILE_DEFINITIONS BULKGRIDTYPE=Dune::UGGrid<3>)

dune_symlink_to_source_files(FILES "params.input" "root8.msh")
