/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#include <vector>
#include <limits>

#include <dune/common/fvector.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dumux/common/parameters.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/multidomain/embedded/circlepoints.hh>

namespace Dumux {

// Compute the distance of p to the segment connecting a->b
template<class Point>
inline auto distanceToSegment(const Point& p, const Point& a, const Point& b)
{
    const auto v = b - a;
    const auto w = p - a;

    const auto proj1 = v*w;
    if (proj1 <= 0.0)
        return w.two_norm();

    const auto proj2 = v.two_norm2();
    if (proj2 <= proj1)
        return (p - b).two_norm();

    const auto t = proj1 / proj2;
    auto x = a;
    x.axpy(t, v);
    return (x - p).two_norm();
}

// Compute the distance of p to the bounding box [min, max]
template<class Point>
inline auto distanceToBoundingBox(const Point& p, const Point& min, const Point& max)
{
    const auto dmax = p-max;
    const auto dmin = min-p;
    auto mindist = dmin[0];
    using std::abs;
    for (int i = 1; i < Point::dimension; ++i)
        if (abs(dmin[i]) < abs(mindist))
            mindist = dmin[i];
    for (int i = 0; i < Point::dimension; ++i)
        if (abs(dmax[i]) < abs(mindist))
            mindist = dmax[i];
    return mindist;
}

class SoilRootDomain
{
public:
    using Point = Dune::FieldVector<double, 3>;
    struct Segment { Point a, b; double r; };
    struct Node { Point a; double r = std::numeric_limits<double>::lowest(); };

    // construct the signed distance function for the root mesh
    SoilRootDomain(bool useAnnulus = false)
    {
        spatialScaling_ = getParam<double>("SpatialScaling", 1.0);
        lowerLeft_ = getParam<Point>("Soil.Grid.LowerLeft")*spatialScaling_;
        upperRight_ = getParam<Point>("Soil.Grid.UpperRight")*spatialScaling_;

        bool resizeBoundingBoxAuto = getParam<bool>("ResizeBoundingBoxAuto", false);
        if (resizeBoundingBoxAuto)
        {
            const auto diag = (upperRight_-lowerLeft_);
            lowerLeft_ += 0.5*diag;
            upperRight_ -= 0.5*diag;
        }

        useAnnulus_ = useAnnulus;
        if (useAnnulus_)
            annulusFactor_ = getParam<double>("Root.Grid.AreaFactor", 1.0);

        // construct network grid
        GridManager<Dune::FoamGrid<1, 3>> networkGridManager;
        networkGridManager.init("Root");
        const auto& networkGridView = networkGridManager.grid().leafGridView();
        auto gridData = networkGridManager.getGridData();

        const auto radiusThreshold = getParam<double>("Root.Grid.RadiusThreshold", 0.0)*spatialScaling_;

        // precompute segment list with radius
        const auto paramType = getParam<std::string>("Root.Grid.DGFParam", "Radius");
        for (const auto& element : elements(networkGridView))
        {
            const auto geometry = element.geometry();
            //const auto eIdx = networkGridView.indexSet().index(element);
            auto level0element = element;
            while (level0element.hasFather())
                level0element = level0element.father();

            const auto rawRadius = [&]{
                if (paramType == "Surface")
                {
                    // see e.g. lupine.dgf
                    const auto rootLength = element.geometry().volume();
                    const auto rootSurface = gridData->parameters(level0element)[2]/(1 << element.level());
                    return rootSurface / rootLength / 2.0 / M_PI;
                }
                else if (paramType == "Radius")
                {
                    static const auto radiusIdx = getParam<int>("Root.Grid.DGFRadiusIndex", 0);
                    static const auto radiusScaling = getParam<double>("Root.Grid.DGFRadiusScaling", 1.0);
                    return gridData->parameters(level0element)[radiusIdx]*radiusScaling;
                }
                else
                    DUNE_THROW(Dune::NotImplemented, "Unknown DGF Parameter " << paramType);
            }();

            const auto radius = useAnnulus_ ? rawRadius*std::sqrt(annulusFactor_ + 1.0)*spatialScaling_ : rawRadius*spatialScaling_;

            // insert segment if radius is larger than the threshold
            if (radius > radiusThreshold)
            {
                segments_.emplace_back(Segment{
                    geometry.corner(0)*spatialScaling_,
                    geometry.corner(1)*spatialScaling_,
                    radius
                });
                // for (const auto& intersection : intersections(networkGridView, element))
                // {
                //     using std::max;
                //     const auto vIdx = networkGridView.indexSet().subIndex(element, intersection.indexInInside(), 1);
                //     nodes_[vIdx].a = geometry.corner(intersection.indexInInside());
                //     nodes_[vIdx].r = max(nodes_[vIdx].r, segments_[eIdx].r);
                // }
            }

            // determine bounding box with boundary points
            for (const auto& intersection : intersections(networkGridView, element))
            {
                if (intersection.boundary())
                {
                    auto point = intersection.geometry().corner(0)*spatialScaling_;

                    if (resizeBoundingBoxAuto)
                    {
                        for (int i = 0; i < point.size(); i++)
                        {
                            lowerLeft_[i] = std::min(point[i], lowerLeft_[i]);
                            upperRight_[i] = std::max(point[i], upperRight_[i]);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < point.size(); i++)
                            point[i] = std::clamp(point[i], lowerLeft_[i], upperRight_[i]);

                    }

                    boundaryIntersections_.emplace_back(point, radius, intersection.centerUnitOuterNormal());
                }
            }
        }
    }

    //! a level set function describing the domain
    double levelSet(const Point& p) const
    {
        if (!(p[0] < upperRight_[0] && p[0] > lowerLeft_[0] &&
              p[1] < upperRight_[1] && p[1] > lowerLeft_[1] &&
              p[2] < upperRight_[2] && p[2] > lowerLeft_[2]))
            return 1.0;

        for (const auto& segment : segments_)
        {
            const auto distS = distanceToSegment(p, segment.a, segment.b)-segment.r;
            if (std::signbit(distS))
                return 1.0;
        }

        return -1.0;
    }

    //! a function describing the cell size field
    double h(const Point& p, double defaultCellSize, double refinement) const
    {
        double minDist = std::numeric_limits<double>::max();
        double maxR = 0.0;
        using std::signbit; using std::min; using std::max;
        for (const auto& segment : segments_)
        {
            const auto distS = distanceToSegment(p, segment.a, segment.b)-segment.r;
            if (signbit(distS))
                return defaultCellSize*refinement;
            else
            {
                minDist = min(minDist, distS);
                maxR = max(maxR, segment.r);
            }
        }

        using std::sqrt;
        return min(refinement*defaultCellSize + defaultCellSize*(1.0-refinement)/(1.5*defaultCellSize)*minDist, defaultCellSize);
    }

    //! a function returning 0 when inside root and minimum distance to a root surface otherwise
    double minRootDistance(const Point& p) const
    {
        double minDist = std::numeric_limits<double>::max();
        using std::signbit; using std::min; using std::max;
        for (const auto& segment : segments_)
        {
            const auto distS = distanceToSegment(p, segment.a, segment.b)-segment.r;
            if (signbit(distS))
                return 0.0;
            else
                minDist = min(minDist, distS);
        }

        return minDist;
    }

    //! a function returning 0 when inside root and minimum distance to a root surface otherwise
    std::pair<double, double> minRootDistanceAndRadius(const Point& p) const
    {
        double minDist = std::numeric_limits<double>::max();
        double maxR = 0.0;
        using std::signbit; using std::min; using std::max;
        for (const auto& segment : segments_)
        {
            const auto distS = distanceToSegment(p, segment.a, segment.b)-segment.r;
            if (signbit(distS))
                return std::make_pair(minDist, segment.r);
            else
            {
                minDist = min(minDist, distS);
                maxR = max(maxR, segment.r);
            }
        }

        return std::make_pair(minDist, maxR);
    }

    std::vector<std::vector<Point>> computeBoundaryIntersections() const
    {
        const auto sincos = EmbeddedCoupling::circlePointsSinCos(360);
        std::vector<std::vector<Point>> boundaryCircles;
        boundaryCircles.resize(boundaryIntersections_.size());
        for (int i = 0; i < boundaryCircles.size(); ++i)
        {
            const auto& [center, radius, normal] = boundaryIntersections_[i];
            EmbeddedCoupling::circlePoints(boundaryCircles[i], sincos, center, normal, radius);
        }
        return boundaryCircles;
    }

    const std::vector<Segment>& segments() const
    { return segments_; }

    // const std::vector<Node>& nodes() const
    // { return nodes_; }

    const Point& lowerLeft() const
    { return lowerLeft_; }

    const Point& upperRight() const
    { return upperRight_; }

private:


    std::vector<Segment> segments_; // internal representation of the root grid
    std::vector<std::tuple<Point, double, Point>> boundaryIntersections_;
    // std::vector<Node> nodes_;
    Point lowerLeft_, upperRight_; // internal representation of the soil grid (bounding box)
    double spatialScaling_;
    double annulusFactor_;
    bool useAnnulus_;
};

} // end namespace Dumux
