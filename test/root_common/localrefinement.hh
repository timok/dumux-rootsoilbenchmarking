/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Class for handling local refinement around the root
 */

#include <vector>
#include <limits>

#include <dune/common/fvector.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dumux/geometry/diameter.hh>

#include "soilrootdomain.hh"

namespace Dumux {

template<class SoilGrid>
void refineAroundRoot(SoilGrid& soilGrid, int maxLevel, double radiusFactor = 2.0)
{
    SoilRootDomain soilRoot;
    for (int level = 0; level < maxLevel; ++level)
        refineAroundRoot(soilRoot, soilGrid, radiusFactor);
}

template<class SoilGrid>
void refineAroundRoot(const SoilRootDomain& soilRoot, SoilGrid& soilGrid, double radiusFactor = 2.0)
{
    int marked = 0;
    soilGrid.preAdapt();
    for (const auto& element : elements(soilGrid.leafGridView()))
    {
        const auto geo = element.geometry();
        const auto center = geo.center();
        const auto h = diameter(geo);
        const auto distInfo = soilRoot.minRootDistanceAndRadius(center);
        const auto& dist = distInfo.first;
        const auto& radius = distInfo.second;

        if (dist < 0.5*h || dist < radiusFactor*radius)
        {
            soilGrid.mark(1, element);
            ++marked;
        }
    }
    soilGrid.adapt();
    soilGrid.postAdapt();

    std::cout << "Refined " << marked << " elements!" << std::endl;
}

} // end namespace Dumux
