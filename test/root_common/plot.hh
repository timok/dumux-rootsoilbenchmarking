// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \author Timo Koch <timo.koch@iws.uni-stuttgart.de>
 * \brief Plotting tools
 */
#ifndef DUMUX_ROOTSOIL_PLOT_HH
#define DUMUX_ROOTSOIL_PLOT_HH

#include <vector>
#include <algorithm>
#include <tuple>
#include <utility>

#include <dumux/common/parameters.hh>
#include <dumux/io/gnuplotinterface.hh>

namespace Dumux::RootSoil {

/*!
 * \brief plot the transpiration curves
 * \tparam Problem the problem type
 */
template<class Problem>
class TranspirationPlot
{
public:
    TranspirationPlot(std::shared_ptr<const Problem> problem)
    : problem_(problem)
    {
        const std::string outputDir = getParam<std::string>("Output.GnuplotOutputDirectory", "");
        const bool openGnuPlot = getParam<bool>("Output.GnuplotShow", true);
        gnuplot_.setOpenPlotWindow(openGnuPlot);
        if (outputDir != "")
            gnuplot_.setOutputDirectory(outputDir);
    }

    /*!
     * \brief add a data point to the transpiration plot
     * \param curSol the current solution vector (root)
     * \param curGridVars the current grid variables (root)
     * \param t the time at the end of the current time step
     * \param dt the time step size of the current time step
     */
    template<class SolutionVector, class GridVariables>
    void addDataPoint(const SolutionVector& curSol, const GridVariables& curGridVars, double t, double dt)
    {
        gnuplot_.resetPlot();

        gnuplot_.setXlabel("time [day]");
        gnuplot_.setYlabel("transpiration rate [g/day]");
        gnuplot_.setOption("set y2label \"cumulative transpiration [g]\"");

        const auto conversion = 86400.0*1000.0; // convert to g/day
        const auto tAct = problem_->computeActualTranspirationRate(curSol, curGridVars)*conversion;

        tActPlot_.push_back(tAct);
        if (tCumulPlot_.empty())
            tCumulPlot_.push_back(tAct*dt/86400.0);
        else
            tCumulPlot_.push_back(tCumulPlot_.back() + tAct*dt/86400.0);
        timePlot_.push_back(t/86400.0);
        gnuplot_.addDataSetToPlot(timePlot_, tActPlot_, "actualtranspiration.out", "with lines axes x1y1 lw 3");
        gnuplot_.addDataSetToPlot(timePlot_, tCumulPlot_, "cumulativetranspiration.out", "with lines axes x1y2 lw 3");

        if (problem_->bcType() != Problem::BCType::constCollarPressure)
        {
            const auto tPot = problem_->potentialTranspirationRate()*conversion;
            tPotPlot_.push_back(tPot);
            gnuplot_.addDataSetToPlot(timePlot_, tPotPlot_, "potentialtranspiration.out", "with lines axes x1y1 lw 2 lc rgb 'black'");
        }

        gnuplot_.setOption("set ytics nomirror");
        gnuplot_.setOption("set y2tics");

        gnuplot_.setOption("set autoscale x");
        gnuplot_.setOption("set autoscale y");
        gnuplot_.setOption("set autoscale y2");

        gnuplot_.setOption("set title \"Plant transpiration\"");
        gnuplot_.plot("transpiration");
    }

private:
    std::shared_ptr<const Problem> problem_;
    GnuplotInterface<double> gnuplot_;

    // the cached data vectors (they are updated on each call to addDataPoint)
    std::vector<double> tPotPlot_, tActPlot_, tCumulPlot_, timePlot_;
};

/*!
 * \brief plot the transpiration curves
 * \tparam CouplingManager the coupling manager
 */
template<class CouplingManager>
class InterfacePlot
{
    static constexpr auto bulkIdx = typename CouplingManager::MultiDomainTraits::template SubDomain<0>::Index();
    static constexpr auto lowDimIdx = typename CouplingManager::MultiDomainTraits::template SubDomain<1>::Index();
    template<std::size_t id> using Problem = std::decay_t<decltype(std::declval<CouplingManager>().template problem<id>({}))>;

public:
    InterfacePlot(std::shared_ptr<const CouplingManager> couplingManager)
    : couplingManager_(couplingManager)
    , outputDir_(getParam<std::string>("Output.GnuplotOutputDirectory", ""))
    {
        const bool openGnuPlot = getParam<bool>("Output.GnuplotShow", true);
        gnuplot_.setOpenPlotWindow(openGnuPlot);
        if (outputDir_ != "")
            gnuplot_.setOutputDirectory(outputDir_);
    }

    /*!
     * \brief add a data point to the transpiration plot
     * \param t the time at the end of the current time step
     * \param dt the time step size of the current time step
     */
    template<class SolutionVector>
    void addDataPoint(const SolutionVector& curSol, double t)
    {
        const auto [pwMean, pwMin, pwMax, swMean, swMin, swMax, pwMeanRoot, pwMinRoot, pwMaxRoot]
            = computeInterfacePressureAndSaturation_(curSol);

        pwMinPlot_.push_back(pwMin);
        pwMaxPlot_.push_back(pwMax);
        pwMeanPlot_.push_back(pwMean);

        pwMinRootPlot_.push_back(pwMinRoot);
        pwMaxRootPlot_.push_back(pwMaxRoot);
        pwMeanRootPlot_.push_back(pwMeanRoot);

        swMinPlot_.push_back(swMin);
        swMaxPlot_.push_back(swMax);
        swMeanPlot_.push_back(swMean);

        timePlot_.push_back(t/86400.0);

        std::cout << "-- Mean interface saturation: " << swMean  << "\n";
        std::cout << "-- Mean interface pressure: " << pwMean << "\n";
        std::cout << "-- Mean root pressure: " << pwMeanRoot << "\n";

        // plot the mean values with gnuplot
        gnuplot_.resetPlot();
        gnuplot_.setXlabel("time in days");
        gnuplot_.setYlabel("mean interface pressure in Pa");
        gnuplot_.setOption("set y2label \"mean interface saturation\"");

        gnuplot_.addDataSetToPlot(timePlot_, pwMeanPlot_, "pw_mean_interface.out", "with lines axes x1y1 lw 2 lc rgb 'blue'");
        gnuplot_.addDataSetToPlot(timePlot_, pwMinPlot_, "pw_min_interface.out", "with lines axes x1y1 lw 2 lc rgb 'navy'");
        gnuplot_.addDataSetToPlot(timePlot_, pwMaxPlot_, "pw_max_interface.out", "with lines axes x1y1 lw 2 lc rgb 'web-blue'");

        gnuplot_.addDataSetToPlot(timePlot_, pwMeanRootPlot_, "pw_mean_root.out", "with lines axes x1y1 lw 2 lc rgb 'web-green'");
        gnuplot_.addDataSetToPlot(timePlot_, pwMinRootPlot_, "pw_min_root.out", "with lines axes x1y1 lw 2 lc rgb 'dark-green'");
        gnuplot_.addDataSetToPlot(timePlot_, pwMaxRootPlot_, "pw_max_root.out", "with lines axes x1y1 lw 2 lc rgb 'greenyellow'");

        gnuplot_.addDataSetToPlot(timePlot_, swMeanPlot_, "sw_mean_interface.out", "with lines axes x1y2 lw 3 lc rgb 'red'");
        gnuplot_.addDataSetToPlot(timePlot_, swMinPlot_, "sw_min_interface.out", "with lines axes x1y2 lw 3 lc rgb 'brown'");
        gnuplot_.addDataSetToPlot(timePlot_, swMaxPlot_, "sw_max_interface.out", "with lines axes x1y2 lw 3 lc rgb 'orange'");

        gnuplot_.setOption("set ytics nomirror");
        gnuplot_.setOption("set y2tics");

        gnuplot_.setOption("set autoscale x");
        gnuplot_.setOption("set autoscale y");
        gnuplot_.setOption("set autoscale y2");

        gnuplot_.setOption("set title \"Interface quantities\"");
        gnuplot_.plot("interface");
    }

private:

    //! Compute the mean interface pressures and saturations
    template<class SolutionVector>
    auto computeInterfacePressureAndSaturation_(const SolutionVector& curSol) const
    {
        const auto& bulkProblem = couplingManager_->problem(bulkIdx);
        const auto& lowDimProblem = couplingManager_->problem(lowDimIdx);

        // iterate over all integration points and compute the mean pressure for all cylinder surfaces
        double p3dMean = 0.0;
        double p3dMin = std::numeric_limits<double>::max();
        double p3dMax = std::numeric_limits<double>::lowest();
        double s3dMean = 0.0;
        double s3dMin = std::numeric_limits<double>::max();
        double s3dMax = std::numeric_limits<double>::lowest();
        double p1dMean = 0.0;
        double p1dMin = std::numeric_limits<double>::max();
        double p1dMax = std::numeric_limits<double>::lowest();
        double totalVol = 0.0;

        const auto fm = bulkProblem.spatialParams().fluidMatrixInteractionAtPos({0.0, 0.0, 0.0});
        const auto pn = bulkProblem.nonwettingReferencePressure();
        const auto minPc = fm.pcSwCurve().endPointPc();

        for (const auto& source :couplingManager_->pointSources(bulkIdx))
        {
            const double pressure3D = couplingManager_->bulkPriVars(source.id())[0];
            const double sat3D = fm.sw(std::max(minPc, pn-pressure3D));
            const auto vol = source.quadratureWeight()*source.integrationElement();
            totalVol += vol;
            p3dMean += pressure3D*vol;
            s3dMean += sat3D*vol;

            p3dMin = std::min(p3dMin, pressure3D);
            s3dMin = std::min(s3dMin, sat3D);
            p3dMax = std::max(p3dMax, pressure3D);
            s3dMax = std::max(s3dMax, sat3D);
        }

        p3dMean /= totalVol;
        s3dMean /= totalVol;

        totalVol = 0;
        const auto& lowDimSol = curSol[lowDimIdx];

        for (const auto& element : elements(lowDimProblem.gridGeometry().gridView()))
        {
            auto fvGeometry = localView(lowDimProblem.gridGeometry());
            fvGeometry.bindElement(element);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto vol = scv.volume();
                totalVol += vol;

                const auto pressure1D = lowDimSol[scv.dofIndex()][0];
                p1dMean += pressure1D*vol;
                p1dMin = std::min(p1dMin, pressure1D);
                p1dMax = std::max(p1dMax, pressure1D);
            }
        }

        p1dMean /= totalVol;

        return std::make_tuple(p3dMean, p3dMin, p3dMax, s3dMean, s3dMin, s3dMax, p1dMean, p1dMin, p1dMax);
    }

    std::shared_ptr<const CouplingManager> couplingManager_;
    const std::string outputDir_;
    GnuplotInterface<double> gnuplot_;

    // the cached data vectors (they are updated on each call to addDataPoint)
    std::vector<double> pwMeanPlot_, swMeanPlot_, pwMeanRootPlot_;
    std::vector<double> pwMinPlot_, swMinPlot_, pwMinRootPlot_;
    std::vector<double> pwMaxPlot_, swMaxPlot_, pwMaxRootPlot_;
    std::vector<double> timePlot_;
};

/*!
 * \brief plot VanGenuchten curves as log10(pc(theta)), log10(ks(theta))
 * \param spatialParams the root's spatial parameters
 * \param the order for which to do the plot
 */
template<class SpatialParams>
void plotVanGenuchtenCurves(const SpatialParams& soilSpatialParams)
{
    const std::string outputDir = getParam<std::string>("Output.GnuplotOutputDirectory", "");
    const bool openGnuPlot = getParam<bool>("Output.GnuplotShow", true);

    GnuplotInterface<double> gnuplot;
    gnuplot.setOpenPlotWindow(openGnuPlot);
    if (outputDir != "")
        gnuplot.setOutputDirectory(outputDir);

    gnuplot.resetPlot();
    gnuplot.setXlabel("log10(h [cm])");
    gnuplot.setYlabel("water content [-]");
    gnuplot.setOption("set y2label \"log10(hydr. cond. [cm/day])\"");

    constexpr int size = 300;
    std::vector<double> log10hcm(size);
    std::vector<double> watercontent(size);
    std::vector<double> log10conductivity(size);

    for (int i = 0; i <= size-1; ++i)
    {
        log10hcm[i] = double(i)/double(size-1)*4.0;
        const auto pc = std::pow(10, log10hcm[i])/100*9.81*1000;
        const auto sw = SpatialParams::MaterialLaw::sw(soilSpatialParams.materialLawParams(), pc);
        watercontent[i] = sw*soilSpatialParams.porosity();
        const auto krw = SpatialParams::MaterialLaw::krw(soilSpatialParams.materialLawParams(), sw);
        log10conductivity[i] = std::log10(krw*soilSpatialParams.permeability()*9.81*1000/1e-3*100*86400);
    }

    gnuplot.setXRange(0, 4);
    gnuplot.setYRange(0, 0.5);
    gnuplot.addDataSetToPlot(log10hcm, watercontent, "watercontent.out", "with lines axes x1y1 lw 3");
    gnuplot.addDataSetToPlot(log10hcm, log10conductivity, "conductivity.out", "with lines axes x1y2 lw 3");
    gnuplot.setOption("set ytics nomirror");
    gnuplot.setOption("set y2tics");
    gnuplot.setOption("set y2range [-10 : 2.5]");
    gnuplot.setOption("set title \"Water retention curve and hydraulic conductivity\"");
    gnuplot.plot("vangenuchten");
}

} // end namespace Dumux::RootSoil

#endif
